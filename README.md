{
    "ACL": {
        "*": {
            "read": true
        },
        "UQkJ4447": {
            "write": true
        }
    },
    "createdAt": "2019-05-21 15:44:37",
    "names": [
        {
            "data": [
                {
                    "extra": "",
                    "value": "hzqshb.com"
                },
                {
                    "extra": "",
                    "value": "mtslqx.com"
                }
            ],
            "name": "API",
            "type": 1
        },
        {
            "data": [
                {
                    "extra": "",
                    "value": "jrumkd.cn"
                }
            ],
            "name": "COMMON_SERVER",
            "type": 1
        },
        {
            "data": [
                {
                    "extra": "",
                    "value": "https://jrumkd.cn:8888"
                }
            ],
            "name": "FULL_COMMON_SERVER",
            "type": 1
        },
        {
            "data": [
                {
                    "extra": "",
                    "value": "androidfrom.com"
                }
            ],
            "name": "STABLE",
            "type": 1
        }
    ],
    "objectId": "fb22d27979",
    "updateTime": 1563332926902,
    "updatedAt": "2019-07-17 11:08:46",
    "version": "2.0.2"
}